﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomLines : MonoBehaviour {

	public Transform leftParent;
	public Transform rightParent;

	public Transform linePrefab;
	public int nLines;
	public int nTries = 10;
	public int seed;

	public Rect rectangle;

	public int leftLayer = 9;
	public int rightLayer = 10;

	void Start () {
		System.Random rng = new System.Random(seed);

		for (int i = 0; i < nLines; i++) {

			int tries = nTries;
			bool collision = true;
			LayerMask mask = LayerMaskExtensions.SingleLayerMask(leftLayer);
			Vector3 p;
			Quaternion q;

			do {
				float x = (float)rng.NextDouble();
				float y = (float)rng.NextDouble();
				p = rectangle.Point(x, y);
				float angle = (float)rng.NextDouble() * 360;
				q = Quaternion.AngleAxis(angle, Vector3.forward);

				Vector2 size = linePrefab.localScale;

				RaycastHit2D hit = Physics2D.BoxCast(p, size, angle, Vector2.zero, 0, mask);

				collision = hit.collider != null;
				tries--;
			} while (collision && tries > 0);



			Transform leftLine = Instantiate(linePrefab);
			leftLine.name = "Left " + i;
			leftLine.parent = leftParent;
			leftLine.localPosition = p;
			leftLine.localRotation = q;
			leftLine.gameObject.layer = leftLayer;

			Transform rightLine = Instantiate(linePrefab);
			rightLine.name = "Right " + i;
			rightLine.parent = rightParent;
			rightLine.localPosition = p;
			rightLine.localRotation = q;
			rightLine.gameObject.layer = rightLayer;

		}
	}
	
	void OnDrawGizmos() {
		Gizmos.color = Color.yellow;
		rectangle.DrawGizmo();		
	}

}
