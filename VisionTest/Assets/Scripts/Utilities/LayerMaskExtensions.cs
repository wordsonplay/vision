﻿using UnityEngine;
using System.Collections;

/**
 * Extensions to Unity's LayerMask class
 * 
 */

public static class LayerMaskExtensions  {

	/**
	 * Check if a particular gameobject is included in the layermask
	 */

	public static bool Contains(this LayerMask layerMask, GameObject gameObject) {
		return (layerMask.value & (1 << gameObject.layer)) != 0;
	}

	/**
	 * Convert a layer number into a layermask
	 */

	public static LayerMask SingleLayerMask(int layer) {
		return 1 << layer;
	}
}